import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default class CreateNote extends Component {
    state = {
        users: [],
        userSelected: '',
        date: new Date(),
        editing: false,
        _id: ''
    }
    async componentDidMount() {
        let res = await axios.get('http://localhost:4000/api/users');
        this.setState({
            users: res.data.map(user => user.userName),
            userSelected: res.data[0].userName
        });
        if (this.props.match.params.id) {
            res = await axios.get('http://localhost:4000/api/notes/'+this.props.match.params.id);
            this.setState({
                title: res.data.title,
                content: res.data.content,
                date: new Date(res.data.date),
                userSelected: res.data.author,
                editing: true,
                _id: this.props.match.params.id
            });

        } 
        

    }

    onSubmit = async e => {
        e.preventDefault();
        const newNote = {
            title: this.state.title,
            content: this.state.content,
            author: this.state.userSelected,
            date: this.state.date
        }
        if(this.state.editing){
            await axios.put('http://localhost:4000/api/notes/'+this.state._id, newNote);
        }else{
            await axios.post('http://localhost:4000/api/notes', newNote);
        }
        
        

        window.location.href = '/';
    }

    onInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onChangeDate = date => {
        this.setState({ date: date });
    }

    render() {
        return (
            <div className="col-md-6 offset-md-3">
                <div className="card card-body">
                    <h4>Create a Note</h4>
                    {/* SELECT USERS */}
                    <div className="form-group">
                        <select
                            name="userSelected"
                            className="form-control"
                            onChange={this.onInputChange}
                            value={this.state.userSelected}>
                            {
                                this.state.users.map(user =>
                                    <option key={user} value={user}>{user}</option>
                                )
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <input
                            className="form-control"
                            name="title"
                            onChange={this.onInputChange}
                            placeholder="Title"
                            required
                            type="text"
                            value={this.state.title} />
                    </div>
                    <div className="form-group">
                        <textarea
                            className="form-control"
                            name="content"
                            onChange={this.onInputChange}
                            placeholder="Content"
                            required
                            value={this.state.content}></textarea>
                    </div>
                    <div className="form-group">
                        <DatePicker
                            className="form-control"
                            selected={this.state.date}
                            onChange={this.onChangeDate} />
                    </div>
                    <form className="" onSubmit={this.onSubmit}>

                        <button className="btn btn-primary" type="submit">Save</button>
                    </form>
                </div>
            </div>
        )
    }
}
