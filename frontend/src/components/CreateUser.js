import React, { Component } from 'react';
import axios from 'axios';

export default class CreateUser extends Component {

    state = {
        users: [],
        userName: ''
    }

    getUsers = async () => {
        const res = await axios.get('http://localhost:4000/api/users');
        this.setState({ users: res.data })
    }
    async componentDidMount() {
        await this.getUsers();
    }

    onChageUserName = (e) => {
        this.setState({ userName: e.target.value });
    }

    onSubmit = async e => {
        e.preventDefault();
        await axios.post('http://localhost:4000/api/users', {
            userName: this.state.userName
        });

        await this.getUsers();
        this.setState({ userName: '' });
    }

    deleteUser = async (id) => {
        await axios.delete('http://localhost:4000/api/users/'+ id);
        await this.getUsers();
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-4">
                    <div className="card card-body">
                        <h3>Create New User</h3>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <input className="form-control"
                                    onChange={this.onChageUserName}
                                    type="text"
                                    value={this.state.userName} />
                            </div>
                            <button className="btn btn-primary" type="submit">Save</button>
                        </form>
                    </div>
                </div>
                <div className="col-md-8">
                    <ul className="list-group">
                        {
                            this.state.users.map(user =>
                                (<li
                                    className="list-group-item list-group-item-action"
                                    key={user._id}
                                    onDoubleClick={() => this.deleteUser(user._id)}
                                >{user.userName}</li>)
                            )
                        }
                    </ul>
                </div>
            </div>
        )
    }
}
