const userCtrl = {};
const User = require('../models/User');

userCtrl.getUsers = async (req, res) => { 
    var users = await User.find();

    res.json(users);
}

userCtrl.createUser = async (req, res) => { 
    const { userName } = req.body;
    const newUser = new User({userName});

    await newUser.save();

    res.json({message: 'User Saved'});
}

userCtrl.deleteUser = async (req, res) => { 
    await User.findByIdAndRemove(req.params.id);
    
    res.json({message: 'User Deleted'});
}

module.exports = userCtrl;