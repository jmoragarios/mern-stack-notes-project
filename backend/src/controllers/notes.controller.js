const notestCtrl = {};

const Note = require('../models/Note');

notestCtrl.getNotes = async (req, res) => { 
    const notes = await Note.find();
    res.json(notes)
}

notestCtrl.createNote = async (req, res) => {
    const { title, content, author, date } = req.body;
    
    const newNote = new Note({
        title,
        content,
        author,
        date
    });

    await newNote.save();
    
    
    res.json({ message: 'Note Saved'})
}

notestCtrl.updateNote = async (req, res)=> { 
    const { title, content, author } = req.body;
    await Note.findByIdAndUpdate(req.params.id, {
        title,
        author,
        content
    });
    res.json({ message: 'Note Updated'});
}

notestCtrl.deleteNote = async (req, res) => { 

    await Note.findByIdAndRemove(req.params.id);
    res.json({message: 'Note deleted'});
}

notestCtrl.getNote = async (req, res) => { 
    var note = await Note.findById(req.params.id);
    
    res.json(note);
}

module.exports = notestCtrl;